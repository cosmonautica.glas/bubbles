
class Homo_line_2d {

  float a, b, c;

  Homo_line_2d(float _a, float _b, float _c){
    a = _a;
    b = _b;
    c = _c;
  }

  Homo_line_2d(Homo_line_2d _line){

  }

  float get_a(){return a;}
  float get_b(){return b;}
  float get_c(){return c;}
}



  float compute_m(float x1,float y1,float x2,float y2){
    float m=((y2-y1)/(x2-x1));
    return m;
  }

  float compute_n(float x1, float y1, float m){
   float n = y1+m*x1;
    return n;
  }

  float compute_y0(float x1, float m, float n){
    float y0 = (m*x1-n);
    return y0;
  }

  float compute_x0(float y1, float m, float n){
    float x0 = ((y1-n)/m);
    return x0;
  }

  Homo_line_2d get_homo_line_by_2_2dpoints(float x0, float y0){
    Homo_line_2d output = new Homo_line_2d((1/x0), (1/y0), -1);
    return output;
}
